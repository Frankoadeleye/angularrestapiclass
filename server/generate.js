// This generate.js file is to be placed right here in your server folder, don't be confused

var faker = require('faker');

var database = { products: []}; //this refers to the product array in database,json file.
// SO what the faker does is to generate as many json objects as you want for you and then it provides an api which your http can communicate with to fetch the data from the database.json file. By entering 'np run generate', 300 json objects were provided in our generate.js file.

for (var i = 1; i<= 15; i++) {
  database.products.push({
    id: i,
    name: faker.commerce.productName(),
    description: faker.lorem.sentences(),
    price: faker.commerce.price(),
    imageUrl: "https://source.unsplash.com/1600x900/?product",
    quantity: faker.random.number()
  });
}

console.log(JSON.stringify(database));